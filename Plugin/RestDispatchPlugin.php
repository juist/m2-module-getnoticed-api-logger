<?php

namespace GetNoticed\ApiLogging\Plugin;

use Magento\Framework;
use Magento\Webapi;
use GetNoticed\ApiLogging as AL;
use Psr\Log;

class RestDispatchPlugin
{
    /**
     * @var AL\Service\ApiLogModeService
     */
    private $apiLogModeService;

    /**
     * @var AL\Service\FileLoggerService
     */
    private $fileLoggerService;

    /**
     * @var Log\LoggerInterface
     */
    private $logger;

    public function __construct(
        AL\Service\ApiLogModeService $apiLogModeService,
        AL\Service\FileLoggerService $fileLoggerService,
        Log\LoggerInterface $logger
    ) {
        $this->apiLogModeService = $apiLogModeService;
        $this->fileLoggerService = $fileLoggerService;
        $this->logger = $logger;
    }

    /**
     * @param Webapi\Controller\Rest         $subject
     * @param callable                       $proceed
     * @param Framework\App\RequestInterface $request
     *
     * @return Framework\App\ResponseInterface
     */
    public function aroundDispatch(
        Webapi\Controller\Rest $subject,
        callable $proceed,
        Framework\App\RequestInterface $request
    ) {
        if ($this->apiLogModeService->isEnabled() !== true) {
            return $proceed($request);
        }

        $response = $proceed($request);

        try {
            $this->fileLoggerService->logRequest($request, $response);
        } catch (Framework\Exception\FileSystemException $e) {
            $this->logger->critical($e->getMessage());
        }

        return $response;
    }
}
