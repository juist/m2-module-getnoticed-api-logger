<?php

namespace GetNoticed\ApiLogging\Command;

use GetNoticed\ApiLogging as AL;
use Symfony\Component\Console;

class ApiLoggingDisableCommand extends AL\Command\AbstractApiLoggingModeCommand
{
    public function configure()
    {
        $this->setName(sprintf('%s:disable', self::COMMAND_PREFIX));
    }

    public function execute(
        Console\Input\InputInterface $input,
        Console\Output\OutputInterface $output
    ) {
        $io = new Console\Style\SymfonyStyle($input, $output);

        if ($this->getApiLogModeService()->isEnabled() !== true) {
            $io->note('API logging is already disabled.');
        } else {
            $this->getApiLogModeService()->setIsEnabled(false);

            $io->success('API logging disabled.');
        }
    }
}
