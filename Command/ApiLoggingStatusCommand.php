<?php

namespace GetNoticed\ApiLogging\Command;

use GetNoticed\ApiLogging as AL;
use Symfony\Component\Console;

class ApiLoggingStatusCommand extends AL\Command\AbstractApiLoggingModeCommand
{
    public function configure()
    {
        $this->setName(sprintf('%s:status', self::COMMAND_PREFIX));
    }

    public function execute(
        Console\Input\InputInterface $input,
        Console\Output\OutputInterface $output
    ) {
        $io = new Console\Style\SymfonyStyle($input, $output);

        if ($this->getApiLogModeService()->isEnabled()) {
            $io->success('API logging is currently enabled.');
        } else {
            $io->success('API logging is currently disabled.');
        }
    }
}
