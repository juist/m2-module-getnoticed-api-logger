<?php

namespace GetNoticed\ApiLogging\Command;

use GetNoticed\ApiLogging as AL;
use Symfony\Component\Console;

class ApiLoggingEnableCommand extends AL\Command\AbstractApiLoggingModeCommand
{
    public function configure()
    {
        $this->setName(sprintf('%s:enable', self::COMMAND_PREFIX));
    }

    public function execute(
        Console\Input\InputInterface $input,
        Console\Output\OutputInterface $output
    ) {
        $io = new Console\Style\SymfonyStyle($input, $output);

        if ($this->getApiLogModeService()->isEnabled()) {
            $io->note('API logging is already enabled.');
        } else {
            $this->getApiLogModeService()->setIsEnabled(true);

            $io->success('API logging enabled.');
        }
    }
}
