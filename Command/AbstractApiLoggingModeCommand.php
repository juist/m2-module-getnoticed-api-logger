<?php

namespace GetNoticed\ApiLogging\Command;

use GetNoticed\ApiLogging as AL;
use Symfony\Component\Console;

abstract class AbstractApiLoggingModeCommand extends Console\Command\Command
{
    const COMMAND_PREFIX = 'api-logging';

    /**
     * @var AL\Service\ApiLogModeService
     */
    private $apiLogModeService;

    public function __construct(
        AL\Service\ApiLogModeService $apiLogModeService,
        ?string $name = null
    ) {
        parent::__construct($name);

        $this->apiLogModeService = $apiLogModeService;
    }

    /**
     * @return AL\Service\ApiLogModeService
     */
    public function getApiLogModeService(): AL\Service\ApiLogModeService
    {
        return $this->apiLogModeService;
    }
}
