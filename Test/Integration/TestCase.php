<?php

namespace GetNoticed\ApiLogging\Test\Integration;

use GetNoticed\ApiLogging\Test;

use PHPUnit\Framework;
use Magento\TestFramework\ObjectManager;

class TestCase
    extends Framework\TestCase
    implements Test\TestCaseInterface
{

    /**
     * @var \Magento\TestFramework\ObjectManager
     */
    protected $_objectManager;

    public function setUp()
    {
        $this->_objectManager = ObjectManager::getInstance();
    }

    public function create($type, $arguments = [])
    {
        return $this->_objectManager->create($type, $arguments);
    }

    public function getTestDirBase(): string
    {
        return realpath(__DIR__) . DIRECTORY_SEPARATOR;
    }

    public function getTestDirFiles(): string
    {
        return $this->getTestDirBase() . '_files' . DIRECTORY_SEPARATOR;
    }

}