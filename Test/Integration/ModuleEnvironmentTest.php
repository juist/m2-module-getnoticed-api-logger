<?php

namespace GetNoticed\ApiLogging\Test\Integration;

use Magento\Framework;

class ModuleEnvironmentTest
    extends TestCase
{

    private $moduleName = 'GetNoticed_ApiLogging';

    public function testExpectsTheModuleToBeRegisteredWithComponentRegistrar()
    {
        $registrar = new Framework\Component\ComponentRegistrar();
        $this->assertArrayHasKey(
            $this->moduleName,
            $registrar->getPaths(
                Framework\Component\ComponentRegistrar::MODULE
            )
        );
    }

    public function testExpectsTheModuleIsConfiguredAndEnabledInTheTestEnvironment()
    {
        $moduleList = $this->create(Framework\Module\ModuleList::class);

        $this->assertTrue(
            $moduleList->has($this->moduleName),
            'The module is not enabled in the test environment.' . PHP_EOL .
            'Check app/etc/config.php and remove all files in /dev/tests/integration/tmp/'
        );
    }

    public function testExpectsTheModuleIsConfiguredAndEnabledInTheRealEnvironment()
    {
        $dirList = $this->create(Framework\App\Filesystem\DirectoryList::class, ['root' => BP]);
        $configReader = $this->create(Framework\App\DeploymentConfig\Reader::class, ['directoryList' => $dirList]);
        $deploymentConfig = $this->create(Framework\App\DeploymentConfig::class, ['reader' => $configReader]);
        $moduleList = $this->create(Framework\Module\ModuleList::class, ['config' => $deploymentConfig]);

        $this->assertTrue(
            $moduleList->has($this->moduleName),
            'The module is not enabled in the real environment.' . PHP_EOL .
            'Check app/etc/config.php and/or run: bin/magento module:enable ' . $this->moduleName
        );
    }

}