<?php

namespace GetNoticed\ApiLogging\Test\Integration;

use Magento\TestFramework\ObjectManager;

class TestCaseTest
    extends TestCase
{

    public function testExpectsTheObjectManagerForUnitTestsToHaveTheRightType()
    {
        // Unit tests           = \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
        // Integration tests    = \Magento\TestFramework\ObjectManager

        $this->assertInstanceOf(ObjectManager::class, $this->_objectManager);
    }

}