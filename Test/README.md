# Magento 2 - GetNoticed ApiLogging

# File is automatically generated, please change TEMPLATE.md!

## Unit Tests

## Setup Unit Tests

1. Rename $MAGENTO_ROOT/dev/tests/unit/phpunit.xml.dist to phpunit.xml
2. Add test in phpunit.xml

```
<testsuite name="ApiLogging">
    <directory suffix="Test.php">../../../app/code/GetNoticed/ApiLogging/Test/Unit</directory>
    <directory suffix="Test.php">../../../vendor/GetNoticed/ApiLogging/Test/Unit</directory>
</testsuite>
```

### Running Unit Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/unit/
../../../vendor/bin/phpunit
```

### View Unit Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/unit/
../../../vendor/bin/phpunit --suite ApiLogging --testdox
```

## Integration Tests

### Setup Integration Tests

1. Rename $MAGENTO_ROOT/dev/tests/integration/phpunit.xml.dist to phpunit.xml
2. Add test in phpunit.xml

```
<testsuite name="ApiLogging">
    <directory suffix="Test.php">../../../app/code/GetNoticed/ApiLogging/Test/Integration</directory>
    <directory suffix="Test.php">../../../vendor/GetNoticed/ApiLogging/Test/Integration</directory>
</testsuite>
```

### Running Integration Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/integration/
../../../vendor/bin/phpunit --suite ApiLogging
```

### View Integration Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/integration/
../../../vendor/bin/phpunit --testdox
```

# Test Results

****
## Test Passed - Wed Dec 20, 2017 12:36
****

### Tests

#### Unit Tests

```
GetNoticed\ApiLogging\Test\Unit\TestCase
 [x] Expects the object manager for unit tests to have the right type

```
#### Integration Tests

```
GetNoticed\ApiLogging\Test\Integration\ModuleEnvironment
 [x] Expects the module to be registered with component registrar
 [x] Expects the module is configured and enabled in the test environment
 [x] Expects the module is configured and enabled in the real environment

GetNoticed\ApiLogging\Test\Integration\TestCase
 [x] Expects the object manager for unit tests to have the right type


=== Memory Usage System Stats ===
Memory usage (OS):	72.59M (164.97% of 44.00M reported by PHP)
Estimated memory leak:	28.59M (39.38% of used memory)

```

### Test results

#### Unit test results

```
.                                                                   1 / 1 (100%)

Time: 177 ms, Memory: 6.00MB

OK (1 test, 1 assertion)
```

#### Integration test results

```
....                                                                4 / 4 (100%)

Time: 12.58 seconds, Memory: 44.00MB

OK (4 tests, 4 assertions)

=== Memory Usage System Stats ===
Memory usage (OS):	72.41M (164.58% of 44.00M reported by PHP)
Estimated memory leak:	28.41M (39.24% of used memory)

```