<?php

// Configuration
define('TEST_SUITE', 'ApiLogging');
define('PHP_UNIT_PATH', '../../../../../vendor/bin');
define('MAGENTO_TESTS_DIR', '../../../../../dev/tests');

// Run tests
chdir(__DIR__);
list($exitCodeUnitTests, $listTestOutputUnit, $testResultOutputUnit) = getTestsAndResults(
    'unit', TEST_SUITE
);
list($exitCodeIntegrationTests, $listTestOutputIntegration, $testResultOutputIntegration) = getTestsAndResults(
    'integration', TEST_SUITE
);

// Check if all tests pass
$passedTests = ($exitCodeUnitTests and $exitCodeIntegrationTests);
$passedText = $passedTests ? 'Passed' : 'FAILED';
$date = date("D M d, Y G:i");
$fileTemplate = file_exists(__DIR__ . '/TEMPLATE.md') ? file_get_contents(__DIR__ . '/TEMPLATE.md') : '';
$vars = [
    '{$passedText}'                  => $passedText,
    '{$date}'                        => $date,
    '{$listTestOutputUnit}'          => $listTestOutputUnit,
    '{$listTestOutputIntegration}'   => $listTestOutputIntegration,
    '{$testResultOutputUnit}'        => $testResultOutputUnit,
    '{$testResultOutputIntegration}' => $testResultOutputIntegration,
    '{$warningMessage}'              => '# File is automatically generated, please change TEMPLATE.md!'
];

// Write new README file
$readmeContents = strtr($fileTemplate, $vars);
file_put_contents(__DIR__ . '/README.md', $readmeContents, LOCK_EX);

// Show test results in console
echo PHP_EOL, PHP_EOL, $testResultOutputUnit, PHP_EOL, PHP_EOL, $testResultOutputIntegration, PHP_EOL;

// Return proper exit code
exit($passedTests ? 0 : 1);

// Functions
function getTestsAndResults(
    $phpUnitConfig,
    $phpUnitSuite,
    $phpUnitDir = PHP_UNIT_PATH,
    $magentoTestDir = MAGENTO_TESTS_DIR
) {
    // Set paths
    $phpUnitDir = realpath(__DIR__ . '/' . $phpUnitDir);
    $testDir = realpath(__DIR__ . '/' . $magentoTestDir . '/' . $phpUnitConfig);
    chdir($testDir);

    // Run testdox
    $exitCode = 1;
    $output = [];
    exec("{$phpUnitDir}/phpunit --testsuite {$phpUnitSuite} --testdox ", $output, $exitCode);

    if ($exitCode !== 0) {
        $listTestOutput = 'Error generating test dox: ' . PHP_EOL . implode(PHP_EOL, $output);
    } else {
        // Remove extra text-header, if tests are passed. (Nice results)
        array_shift($output);
        array_shift($output);
        $listTestOutput = implode(PHP_EOL, $output);
    }

    // Run regular
    $exitCode = 1;
    $output = [];

    exec("{$phpUnitDir}/phpunit --testsuite {$phpUnitSuite}", $output, $exitCode);

    if ($exitCode == 0) {
        // Remove extra text-header, if tests are passed. (Nice results)
        array_shift($output);
        array_shift($output);
    }

    // Return results
    return [$exitCode == 0, $listTestOutput, implode(PHP_EOL, $output)];
}