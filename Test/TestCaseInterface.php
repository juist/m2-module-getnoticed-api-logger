<?php

namespace GetNoticed\ApiLogging\Test;

interface TestCaseInterface
{

    public function getTestDirBase();

    public function getTestDirFiles();

    public function create($type, $arguments = []);

}