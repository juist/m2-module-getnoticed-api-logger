<?php

namespace GetNoticed\ApiLogging\Test\Unit;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

class TestCaseTest extends TestCase
{

    public function testExpectsTheObjectManagerForUnitTestsToHaveTheRightType()
    {
        $this->assertInstanceOf(ObjectManager::class, $this->_objectManager);
    }

}