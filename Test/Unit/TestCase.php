<?php

namespace GetNoticed\ApiLogging\Test\Unit;

use GetNoticed\ApiLogging\Test;

use PHPUnit\Framework;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

class TestCase
    extends Framework\TestCase
    implements Test\TestCaseInterface
{

    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    protected $_objectManager;

    public function setUp(): void
    {
        parent::setUp();
        $this->_objectManager = new ObjectManager($this);
    }

    public function create($type, $arguments = [])
    {
        return $this->_objectManager->getObject($type, $arguments);
    }

    public function getTestDirBase(): string
    {
        return realpath(__DIR__) . DIRECTORY_SEPARATOR;
    }

    public function getTestDirFiles(): string
    {
        return $this->getTestDirBase() . '_files' . DIRECTORY_SEPARATOR;
    }

    public function getTestFile($filename)
    {
        $filename = $this->getTestDirFiles() . $filename;
        $this->assertTrue(file_exists($filename), sprintf('Test file `%s` does not exists.', $filename));
        $this->assertTrue(
            is_readable($filename),
            sprintf('Test file `%s` is not readable (check permissions).', $filename)
        );

        return $filename;
    }

}
