<?php

namespace GetNoticed\ApiLogging\Service;

use Magento\Framework;
use GetNoticed\ApiLogging as AL;

class FileLoggerService
{
    const LOGGER_DIRECTORY = Framework\App\Filesystem\DirectoryList::LOG;

    /**
     * @var Framework\Filesystem
     */
    private $filesystem;

    /**
     * @var AL\Service\FileFormatterService
     */
    private $fileFormatterService;

    public function __construct(
        Framework\Filesystem $filesystem,
        AL\Service\FileFormatterService $fileFormatterService
    ) {
        $this->filesystem = $filesystem;
        $this->fileFormatterService = $fileFormatterService;
    }

    /**
     * @return Framework\Filesystem\Directory\WriteInterface
     * @throws Framework\Exception\FileSystemException
     */
    public function getLoggerDirectory(): Framework\Filesystem\Directory\WriteInterface
    {
        return $this->filesystem->getDirectoryWrite(self::LOGGER_DIRECTORY);
    }

    /**
     * @param Framework\App\RequestInterface|Framework\App\Request\Http   $request
     * @param Framework\App\ResponseInterface|Framework\App\Response\Http $response
     *
     * @throws Framework\Exception\FileSystemException
     */
    public function logRequest(
        Framework\App\RequestInterface $request,
        Framework\App\ResponseInterface $response
    ): void {
        $fileName = sprintf('call.%d.%s', time(), $this->getRandomFileName());

        $this->getLoggerDirectory()->writeFile(
            sprintf('api-logging/%s', $fileName),
            implode(
                PHP_EOL . str_repeat('=', 100) . PHP_EOL,
                [
                    'API CALL LOG',
                    'REQUEST',
                    sprintf(
                        '%s %s',
                        $request->getMethod(),
                        $request->getRequestUri()
                    ),
                    'REQUEST: HEADERS',
                    $this->fileFormatterService->arrayPrettyPrint($request->getHeaders()->toArray()),
                    'REQUEST: PARAMETERS',
                    $this->fileFormatterService->arrayPrettyPrint($request->getParams()),
                    'REQUEST: POST VALUES',
                    $this->fileFormatterService->arrayPrettyPrint($request->getPost()->toArray()),
                    'REQUEST: CONTENT',
                    $this->fileFormatterService->contentPrettyPrint($request->getContent()),
                    'RESPONSE: HEADERS',
                    $this->fileFormatterService->arrayPrettyPrint($response->getHeaders()->toArray()),
                    'RESPONSE: CONTENT',
                    $this->fileFormatterService->contentPrettyPrint($response->getContent())
                ]
            )
        );
    }

    private function getRandomFileName(int $length = 6): string
    {
        $characters = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        $charactersCount = count($characters);

        $string = [];

        for ($i = 0; $i < $length; $i++) {
            $string[] = $characters[rand(0, $charactersCount - 1)];
        }

        return implode('', $string);
    }
}
