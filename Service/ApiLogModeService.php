<?php

namespace GetNoticed\ApiLogging\Service;

use Magento\Framework;

class ApiLogModeService
{
    const FLAG_FILENAME = '.api-logging.flag';

    const FLAG_DIR = Framework\App\Filesystem\DirectoryList::VAR_DIR;

    /**
     * @var Framework\Filesystem\Directory\WriteInterface
     */
    private $flagDir;

    // DI

    /**
     * @var Framework\Filesystem
     */
    private $filesystem;

    public function __construct(
        Framework\Filesystem $filesystem
    ) {
        $this->filesystem = $filesystem;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        try {
            return $this->getFlagDir()->isExist(self::FLAG_FILENAME);
        } catch (Framework\Exception\FileSystemException $e) {
            return false;
        }
    }

    /**
     * @param bool $enabled
     *
     * @throws Framework\Exception\FileSystemException
     */
    public function setIsEnabled(bool $enabled): void
    {
        if ($enabled === true && $this->flagDir->isExist(self::FLAG_FILENAME) === false) {
            $this->getFlagDir()->touch(self::FLAG_FILENAME);
        }

        if ($enabled !== true && $this->flagDir->isExist(self::FLAG_FILENAME) === true) {
            $this->getFlagDir()->delete(self::FLAG_FILENAME);
        }
    }

    /**
     * @return Framework\Filesystem\Directory\WriteInterface
     * @throws Framework\Exception\FileSystemException
     */
    private function getFlagDir(): Framework\Filesystem\Directory\WriteInterface
    {
        if ($this->flagDir instanceof Framework\Filesystem\Directory\WriteInterface) {
            return $this->flagDir;
        }

        return $this->flagDir = $this->filesystem->getDirectoryWrite(self::FLAG_DIR);
    }
}
