<?php

namespace GetNoticed\ApiLogging\Service;

use Magento\Framework;

class FileFormatterService
{
    /**
     * @var Framework\Serialize\Serializer\Json
     */
    private $jsonSerializer;

    public function __construct(
        Framework\Serialize\Serializer\Json $jsonSerializer
    ) {
        $this->jsonSerializer = $jsonSerializer;
    }

    public function arrayPrettyPrint(array $data, int $indent = 0): string
    {
        $return = [];

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $return[] = sprintf('%s = %s', $key, $this->arrayPrettyPrint($value, $indent + 1));
            } else {
                $return[] = sprintf('%s = %s', $key, $value);
            }
        }

        return implode(PHP_EOL, $return);
    }

    public function contentPrettyPrint($content): string
    {
        try {
            $json = $this->jsonSerializer->unserialize($content);
        } catch (\Exception | \Error $e) {
            if (is_array($content)) {
                return $this->arrayPrettyPrint($content);
            } else {
                return (string)$content;
            }
        }

        return \json_encode($json, JSON_PRETTY_PRINT);
    }
}
