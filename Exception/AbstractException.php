<?php

namespace GetNoticed\ApiLogging\Exception;

use Magento\Framework\Exception;

abstract class AbstractException
    extends Exception\LocalizedException
{

}